%% Make sure paths are set correctly
run('setup_environment.m')

%% Configure a new tracking project
projectName = 'TracX_Yeast_SCerevisiae_Experiment'; % Project name
fileIdentifierFingerprintImages = 'BFdivide'; % Image identifier for Brigthfield images;
fileIdentifierWellPositionFingerprint = []; % Well position identifier if multiwell experiment.
fileIdentifierCellLineage = 'mKate'; % Image identifier for the Cell Lineage reconstruction (i.e bud neck marker).
imageCropCoordinateArray = [82 80 188 183]; % Empty if no crop has been applied in CellX, add CellX cropRegionBoundaries coordinates otherwise (from CellX_SCerevisiae_Parameter.xml).
movieLength = 30; % Number of timepoints to track 
cellsFilePath = fullfile(demoDataPath, 'yeast', 'scerevisiae', 'CellXSegmentation', 'mKO_mKate'); % Path to segmentation results (CellX Style).
imagesFilePath = fullfile(demoDataPath, 'yeast', 'scerevisiae', 'RawImages'); % Path to raw images.
cellDivisionType = 'asym'; % Cell division type.

% Create a tracker instance and a new project
Tracker = TracX.Tracker();
Tracker.createNewTrackingProject(projectName, imagesFilePath, ...
        cellsFilePath, fileIdentifierFingerprintImages, ...
        fileIdentifierWellPositionFingerprint, fileIdentifierCellLineage, ...
        imageCropCoordinateArray, movieLength, cellDivisionType);
Tracker.revertSegmentationImageCrop() % Reverts the potential image crop applied by CellX to map the cell coordinates to the full image.

%% Constrain input by removing false positive segmentations from CellX
Tracker.testSegmentationArtefactRemoval(22, 1, 3)
% Deleted artefacts
Tracker.deleteSegArtefacts(1, 3)
%% Test default tracking parameters
% Optionally tune tracking parameters for better results
Tracker.configuration.ParameterConfiguration.setMaxTrackFrameSkipping(4);
% Tracker.configuration.ParameterConfiguration.setMaxCellSizeDecrease(0.6);
% Tracker.configuration.ParameterConfiguration.setMeanCellDiameter(35);
% Tracker.configuration.ParameterConfiguration.setMaxCellCenterDisplacement(25)
% Tracker.configuration.ParameterConfiguration.setMaxCellSizeIncrease(4)

% Dry run to test the tracking parameters
Tracker.testTrackingParameters([1,30]) % Track from frame 1 to frame 30 for testing.

%% Run the tracker
Tracker.runTracker()

%% Save the tracking results
Tracker.saveCurrentTrackerState() % Saves the tracker state as mat file (to continue work anytime later)
Tracker.saveTrackingProject() % Saves the tracking project.
Tracker.saveTrackerResultsAsTable() % Saves the tracking results as one column seperated table for further analysis.
%%
Tracker.saveTrackerProjectControlImages('isParallel', false, 'maxWorkers', 12) % Save additional control images to inspect the sucess of the tracking.

%% Annotate a given image frame with any data property
% I.e for frame 15 we want to annotate the cell area. Skipping the property
% argument takes the track_index by default to inspect how well the
% tracking went.
Tracker.imageVisualization.plotAnnotatedControlImageFrame(15, 'cell_area')
%% Run optional lineage reconstruction

% Tune the sensitivity threshold until you only detect the true budnecks
% This tests the divisionMarkerEdgeSensitivityThresh=0.14 for frame 15 and
% will show 4 control images. Other parameters key value pairs that can be 
% modified are:
%    'divisionMarkerEdgeSensitivityThresh', 25, 
%    'divisionMarkerConvexAreaLowerThresh', 100,
%    'divisionMarkerConvexAreaUpperThresh', 5,
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMeanObjDiameter(7)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxExpectedMovement(4)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxObjCenterDisplacement(4)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMeanObjDiameterScalingFactor(1);
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxTrackFrameSkipping(1)
Tracker.configuration.ParameterConfiguration.setDivisionMarkerEdgeSensitivityThresh(0.18) 
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxObjSizeDecrease(0.5);
Tracker.configuration.ParameterConfiguration.setDivisionMarkerMaxObjSizeIncrease(4);
Tracker.testBudNeckSegmentation(15, 'divisionMarkerEdgeSensitivityThresh', 0.18)

% Apply the relevant parameters from the test outcome
Tracker.configuration.ParameterConfiguration.setDivisionMarkerEdgeSensitivityThresh(0.18);

%% Test marker tracking
Tracker.testMarkerTrackingParameters([1,30]) 
%%
% Indicate the cell division type, if optional control images should be
% saved and which channel bears the bud neck marker
Tracker.runLineageReconstruction('symmetricalDivision', false, ...
    'WriteControlImages', false, 'nuclearMarkerChannel', 1);
%% 
% Plot linaege for track 1 to console
trackToDisplay = 2;
Tracker.imageVisualization.plotLineageTree(trackToDisplay);
% Plot linaege for track 1 to figure
fh = Tracker.imageVisualization.plotLineageTree(trackToDisplay, 'plotToFigure', true);
fh{trackToDisplay}.Visible = true;

%% Print the cell cycle phase information as table and save it
Tracker.lineage.cellCyclePhaseTable
Tracker.saveTrackerCellCycleResultsAsTable()

%% Generate an animated movie of a lineage of interest
Tracker.imageProcessing.generateLineageMovie(2, 5/60, 'Track21LineageMovie')

%% Save  marker time series control images for lineage roots
% This creates a mask overlay with the budneck marker channel and nuclear
% channel signal
Tracker.imageProcessing.generateMarkerTimeSeriesImage('cycleMarker', ...
    {'mKOk', 'mKate'}, 'isPlotMarkerSignal', true)