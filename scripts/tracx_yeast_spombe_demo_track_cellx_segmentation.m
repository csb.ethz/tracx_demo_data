%% Make sure paths are set correctly
run('setup_environment.m')

%%
% Configure a new tracking project
projectName = 'TracX_Yeast_SPombe_Experiment'; % Project name
fileIdentifierFingerprintImages = 'BFdivide'; % Image identifier for Brigthfield images;
fileIdentifierWellPositionFingerprint = []; % Well position identifier if multiwell experiment.
fileIdentifierCellLineage = ''; % Image identifier for the Cell Lineage reconstruction (i.e bud neck marker).
imageCropCoordinateArray = [136, 164, 526, 524]; % Empty if no crop has been applied in CellX, add CellX cropRegionBoundaries coordinates otherwise (from CellX_SCerevisiae_Parameter.xml).
movieLength = 30; % Number of timepoints to track 
cellsFilePath = fullfile(demoDataPath, 'yeast', 'spombe', 'CellXSegmentation'); % Path to segmentation results (CellX Style).
imagesFilePath = fullfile(demoDataPath, 'yeast', 'spombe', 'RawImages'); % Path to raw images.
cellDivisionType = 'sym'; % Cell division type.

% Create a tracker instance and a new project
Tracker = TracX.Tracker();
Tracker.createNewTrackingProject(projectName, imagesFilePath, ...
        cellsFilePath, fileIdentifierFingerprintImages, ...
        fileIdentifierWellPositionFingerprint, fileIdentifierCellLineage, ...
        imageCropCoordinateArray, movieLength, cellDivisionType);
Tracker.revertSegmentationImageCrop() % Reverts the potential image crop applied by CellX to map the cell coordinates to the full image.

%% Constrain input with cell area and threshold 600
Tracker.testFeatureDataRemoval(30, 'cell_area', 600)
%%
Tracker.deleteByFeature('cell_area', 600)
%% Test default tracking parameters
% Optionally tune tracking parameters for better results
% Tracker.configuration.ParameterConfiguration.setMaxTrackFrameSkipping(3);
% Tracker.configuration.ParameterConfiguration.setMaxCellSizeDecrease(0.6);
% Tracker.configuration.ParameterConfiguration.setMaxCellCenterDisplacement(25)
Tracker.configuration.ParameterConfiguration.setMaxCellSizeIncrease(4)

% Dry run to test the tracking parameters
Tracker.testTrackingParameters([1,10]) % Track from frame 1 to frame 10 for testing.


%% Run the tracker
Tracker.configuration.ParameterConfiguration.setMaxCellSizeIncrease(4)
Tracker.runTracker()

%% Save the tracking results
Tracker.saveCurrentTrackerState() % Saves the tracker state as mat file (to continue work anytime later)
Tracker.saveTrackingProject() % Saves the tracking project.
Tracker.saveTrackerResultsAsTable() % Saves the tracking results as one column seperated table for further analysis.
%%
Tracker.saveTrackerProjectControlImages('isParallel', true, 'maxWorkers', 12) % Save additional control images to inspect the sucess of the tracking.

%% Annotate a given image frame with any data property
% I.e for frame 15 we want to annotate the cell area. Skipping the property
% argument takes the track_index by default to inspect how well the
% tracking went.
Tracker.imageVisualization.plotAnnotatedControlImageFrame(15, 'cell_area')
%% Plot Tracking summary

%% Run optional lineage reconstruction
% Indicate the cell division type, if optional control images should be
% saved and which channel bears the bud neck marker
% Apply the relevant parameters from the test outcome
Tracker.runLineageReconstruction('symmetricalDivision', true, ...
    'WriteControlImages', false);
%% 
% Plot linaege for track 1 to console
Tracker.imageVisualization.plotLineageTree(1);
% Plot linaege for track 1 to figure
fh = Tracker.imageVisualization.plotLineageTree(1, 'plotToFigure', true);
fh{1}.Visible = true;
set(gcf,'color','w');

%% Print the cell cycle phase information as table and save it
Tracker.lineage.cellCyclePhaseTable
%Tracker.saveTrackerCellCycleResultsAsTable()

%% Generate an animated movie of a lineage of interest
Tracker.imageProcessing.generateLineageMovie(1, 5/60, 'Track1LineageMovie')
