# tracx_demo_data

Demo data for tracking software Trac<sup>X</sup>. It will help you to familiarize with the algorithm and showcase different functionalities for the command line interface (CLI). 

This repo has the following structure:

```
/data
    /yeast
        /scerevisiae
        /spombe
/scripts
    /*.m
```



The folder `scerevisiae` showcases the case of asymmetrical cell division.

The folder `spombe` showcases the case of symmetrical cell division of rod shaped cells.



Trac<sup>X</sup> builds on the file format of CellX. Therefore there is allways a CellX demo folder as default. However, Trac<sup>X</sup> also supports generic segmentation masks and is therefore compatible with any segmentation algorithm available (given that such tools can export labeled segmentation masks). Such generic masks need to be first converted into the internal format. All the tools required to convert file formats are included and described in the documentation [here](https://tracx.readthedocs.io/en/latest/start.html#from-labeled-segmentation-masks) and showcased in the attached scripts. The same functionalities are also present in the graphical user interface.
